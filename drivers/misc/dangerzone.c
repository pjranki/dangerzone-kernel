#include <linux/module.h>		// For kernel module + printk
#include <linux/kallsyms.h>		// For kallsyms_lookup_name (think dlsym/GetProcAddress)
#include <linux/pagemap.h>		// For get_kernel_page
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <asm/pgtable.h>
#include <asm/tlbflush.h>
#include <linux/device.h>		// Header to support the kernel Driver Model
#include <linux/fs.h>			// Header for the Linux file system support
#include <linux/uaccess.h>		// Required for the copy to user function
#include <linux/slab.h>			// kalloc
#include <linux/msm_ion.h>		// ion apis and structure

/*------------------------------------------------------------------*/
// HEX DUMP
typedef void (*HxDuPrintFn)(void *ctx, const char *str);
void hxdu_print_nibble(void *print_ctx, HxDuPrintFn print_cb, unsigned char nibble) {
    char buffer[2] = {'\x00', '\x00'};
    if (nibble < 10) {
        buffer[0] = '0' + nibble;
    } else if (nibble < 16) {
        buffer[0] = 'A' + (nibble - 10);
    }
    print_cb(print_ctx, buffer);
}
void hxdu_print_hex(void *print_ctx, HxDuPrintFn print_cb, unsigned char value) {
    hxdu_print_nibble(print_ctx, print_cb, (value >> 4) & 0xf);
    hxdu_print_nibble(print_ctx, print_cb, (value >> 0) & 0xf);
}
void hxdu_hexdump(void *print_ctx, HxDuPrintFn print_cb, const void* data, size_t size) {
	char ascii[17];
	size_t i, j;
	ascii[16] = '\0';
	for (i = 0; i < size; ++i) {
		hxdu_print_hex(print_ctx, print_cb, ((unsigned char*)data)[i]);
        print_cb(print_ctx, " ");
		if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			print_cb(print_ctx, " ");
			if ((i+1) % 16 == 0) {
				print_cb(print_ctx, "|  ");
                print_cb(print_ctx, ascii);
                print_cb(print_ctx, " \n");
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					print_cb(print_ctx, " ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					print_cb(print_ctx, "   ");
				}
				print_cb(print_ctx, "|  ");
                print_cb(print_ctx, ascii);
                print_cb(print_ctx, " \n");
			}
		}
	}
}

void hexdumpk_printk(void *ctx, const char *str) {
    printk(str);
}
#define hexdumpk(data,size)     hxdu_hexdump(NULL,hexdumpk_printk,data,size)


/*------------------------------------------------------------------*/
// File system (because your not suppose to read filesystem from kernel)

#define	FILE_SEEK_SET	0	/* set file offset to offset */
#define	FILE_SEEK_CUR	1	/* set file offset to current plus offset */
#define	FILE_SEEK_END	2	/* set file offset to EOF plus offset */

struct file *file_open(const char *path, int flags, int rights) 
{
    struct file *filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(KERNEL_DS);//set_fs(get_ds());
    filp = filp_open(path, flags, rights);
    set_fs(oldfs);
    if (IS_ERR(filp)) {
        err = PTR_ERR(filp);
        return NULL;
    }
    return filp;
}

int file_read(struct file *file, unsigned long long offset, unsigned char *data, unsigned int size) 
{
    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(KERNEL_DS);//set_fs(get_ds());

    ret = vfs_read(file, data, size, &offset);

    set_fs(oldfs);
    return ret;
}

loff_t file_lseek(struct file *file, loff_t offset, int whence)
{
    mm_segment_t oldfs;
    loff_t ret;

    oldfs = get_fs();
    set_fs(KERNEL_DS);//set_fs(get_ds());

    ret = vfs_llseek(file, offset, whence);

    set_fs(oldfs);
    return ret;
}

void file_close(struct file *file) 
{
    filp_close(file, NULL);
}

/*------------------------------------------------------------------*/
// Convert strings to numbers
u32 atoi_u32_best_guess(const char *number) {
	const char *pos = number;
	size_t len = 0;
	u32 base = 10;
	u32 value = 0;
	if (NULL == number) {
		return 0;
	}
	while ('\0' != *pos) {
		len++;
		pos++;
	}
	if ((len >= 2) && (number[0] == '0') && ((number[1] == 'x') || (number[1] == 'X'))) {
		number += 2;
		len -= 2;
		base = 16;
	}
	if (0 == len) {
		return 0;
	}
	pos = number;
	while (len > 0) {
		if (10 == base) {
			value *= 10;
			if ((*pos >= '0') && (*pos <= '9')) {
				value += (u32)(*pos - '0');
			} else {
				return 0;
			}
		}
		if (16 == base) {
			value <<= 4;
			if ((*pos >= '0') && (*pos <= '9')) {
				value += (u32)(*pos - '0');
			} else if ((*pos >= 'a') && (*pos <= 'f')) {
				value += (u32)((*pos - 'a') + 0xa);
			} else if ((*pos >= 'A') && (*pos <= 'F')) {
				value += (u32)((*pos - 'A') + 0xa);
			} else {
				return 0;
			}
		}
		pos++;
		len--;
	}
	return value;
}

/*------------------------------------------------------------------*/
// AARCH64 (little-endian) disassembly tools
void *aarch64le_get_adrp_addr(void *ins, void *addr_of_ins) {
	unsigned long delta = 0;
	unsigned char ople = 0;
	unsigned char *opbyte = (unsigned char*)ins;
    void *addr = addr_of_ins;
	if (NULL == ins) {
		return NULL;
	}
	if (NULL == addr) {
		// for instructions in-memory
		addr = ins;
	}
	ople = opbyte[3];
	if ((ople == 0x90) || (ople == 0xb0) || (ople == 0xd0) || (ople == 0xf0)) {
		delta = 0;
		delta += ((((unsigned long)(opbyte[3])) >> 5) & 0x3);
		delta += (((((unsigned long)(opbyte[0])) >> 5) & 0x7) << 2);
		delta += (((unsigned long)(opbyte[1])) << 5);
		delta += (((unsigned long)(opbyte[2])) << (5 + 8));
		delta = delta << 12;
		if ((opbyte[2] & 0x80) != 0) {
			delta |= 0xffffffff00000000;
			delta *= -1;
			addr = (void*)((((unsigned long)addr) - delta) & (~0xfff));
		} else {
			addr = (void*)((((unsigned long)addr) + delta) & (~0xfff));
		}
		return addr;
	}
	return NULL;
}
void *aarch64le_get_bl_addr(void *ins, void *addr_of_ins) {
	int delta = 0;
	unsigned char ople = 0;
    void *addr = addr_of_ins;
	if (NULL == ins) {
		return NULL;
	}
	if (NULL == addr) {
		// for instructions in-memory
		addr = ins;
	}
	ople = *((unsigned char *)ins + 3);
	if (ople == 0x97) {
		delta = 0;
		delta |= ((((int)(*((unsigned char*)ins + 0))) << 0) & 0xff);
		delta |= ((((int)(*((unsigned char*)ins + 1))) << 8) & 0xff00);
		delta |= ((((int)(*((unsigned char*)ins + 2))) << 16) & 0xff0000);
		delta &=~ 0xff000000;
		if (delta & 0x00800000) {
			delta |= 0xff000000;
			delta *= -4;
			addr = (void *)(((unsigned long)addr) - ((unsigned long)delta));
		} else {
			delta *= 4;
			addr = (void *)(((unsigned long)addr) + ((unsigned long)delta));
		}
		return addr;
	}
	return NULL;
}
bool aarch64le_b_ins(void *ins, void *addr_of_ins, void *branch_to_here) {
	int delta = 0;
    if (!ins) {
        return false;
    }
	if (!addr_of_ins) {
		addr_of_ins = ins;
	}
	delta = (int)((((unsigned long)branch_to_here) - ((unsigned long)addr_of_ins)) / 4);
	((unsigned char*)ins)[0] = ((unsigned char)(delta >> 0)) & 0xff;
	((unsigned char*)ins)[1] = ((unsigned char)(delta >> 8)) & 0xff;
	((unsigned char*)ins)[2] = ((unsigned char)(delta >> 16)) & 0xff;
	if (delta >= 0) {
		((unsigned char*)ins)[3] = 0x14;
	} else {
		((unsigned char*)ins)[3] = 0x17;
	}
    return true;
}
bool aarch64le_set_literal_to_addr(void *literal_addr, void *addr) {
    size_t i = 0;
    if (!literal_addr) {
        return false;
    }
    for (i = 0; i < 8; i++) {
        ((unsigned char *)literal_addr)[i] = (unsigned char)((((uint64_t)addr) >> (i * 8)) & 0xff);
    }
    return true;
}
bool aarch64le_is_ret(void *ins) {
    unsigned char *ret_ins = "\xC0\x03\x5F\xD6";
    if ((*((uint32_t*)ins)) == (*((uint32_t*)ret_ins))) {
        return true;
    }
	return false;
}
void *aarch64le_mine_adrp_addr(void *data, size_t size, size_t index) {
	void *addr = NULL;
	size_t offset = 0;
	for (offset = 0; offset < size; offset += 4) {
		addr = aarch64le_get_adrp_addr((void*)(((unsigned char*)data) + offset), NULL);
		if (NULL != addr) {
            if (0 == index) {
			    return addr;
            } else {
                index--;
            }
		}
	}
	return addr;
}
void *aarch64le_mine_bl_addr(void *data, size_t size, size_t index) {
	void *addr = NULL;
	size_t offset = 0;
	for (offset = 0; offset < size; offset += 4) {
		addr = aarch64le_get_bl_addr((void*)(((unsigned char*)data) + offset), NULL);
		if (NULL != addr) {
            if (0 == index) {
			    return addr;
            } else {
                index--;
            }
		}
	}
	return addr;
}
void *aarch64le_mine_bl(void *data, size_t size, size_t index) {
	void *addr = NULL;
	size_t offset = 0; 
	for (offset = 0; offset < size; offset += 4) {
		addr = aarch64le_get_bl_addr((void*)(((unsigned char*)data) + offset), NULL);
		if (NULL != addr) {
            if (0 == index) {
			    return (void*)(((unsigned char*)data) + offset);
            } else {
                index--;
            }
		}
	}
	return NULL;
}
void *aarch64le_mine_ret(void *data, size_t size, size_t index) {
	size_t offset = 0;
	for (offset = 0; offset < size; offset += 4) {
		if (aarch64le_is_ret((void*)(((unsigned char*)data) + offset))) {
            if (0 == index) {
			    return (void*)(((unsigned char*)data) + offset);
            } else {
                index--;
            }
		}
	}
	return NULL;
}
size_t aarch64le_addr_diff(void *addr_low, void *addr2_high) {
    if (addr2_high <= addr_low) {
        return 0;
    }
    return (size_t)(addr2_high - addr_low);
}

// THIS IS WHAT MY CALL STUB LOOKS LIKE:
// \x89\x00\x00\x58                    ldr x9, #0x10   # ldr x9, =0xBADC0FFEE0DDF00D
// \x20\x01\x3f\xd6                    blr x9
// \xfb\xff\xff\x17                    b  #xxxxxxxx
// \x1f\x20\x03\xd5                    nop
// \x0d\xf0\xdd\xe0\xfe\x0f\xdc\xba    =0xBADC0FFEE0DDF00D
bool aarch64le_abs_call_stub_x9(void *stub, size_t stub_size, void *addr_of_stub, void *func_to_call, void *return_to_here) {
    void *current_stub = stub;
    void *current_addr_of_stub = addr_of_stub;

    // need 24 bytes for this stub
    if (stub_size < 24) {
        return false;
    }

    // ldr x9, #0x10   # ldr x9, =LITERAL_WITH_ABS_ADDR
    memcpy(current_stub, "\x89\x00\x00\x58", 4);

    // blr x9
    current_stub = (void*)(((unsigned char*)current_stub) + 4);
    current_addr_of_stub = (void*)(((unsigned char*)current_addr_of_stub) + 4);
    memcpy(current_stub, "\x20\x01\x3f\xd6", 4);

    // b #return_to_here
    current_stub = (void*)(((unsigned char*)current_stub) + 4);
    current_addr_of_stub = (void*)(((unsigned char*)current_addr_of_stub) + 4);
    if (!aarch64le_b_ins(current_stub, current_addr_of_stub, return_to_here)) {
        return false;
    }

    // nop  # for 8-byte alignment
    current_stub = (void*)(((unsigned char*)current_stub) + 4);
    current_addr_of_stub = (void*)(((unsigned char*)current_addr_of_stub) + 4);
    memcpy(current_stub, "\x1f\x20\x03\xd5", 4);

    // literal for absolute address
    current_stub = (void*)(((unsigned char*)current_stub) + 4);
    current_addr_of_stub = (void*)(((unsigned char*)current_addr_of_stub) + 4);
    if (!aarch64le_set_literal_to_addr(current_stub, func_to_call)) {
        return false;
    }

    // all is ok
    return true;
}

/*------------------------------------------------------------------*/
// KERNEL WRITE MEMORY
typedef int (*KwSetMemoryFn)(unsigned long addr, int numpages);
typedef struct vm_struct * (*KwFindVmAreaFn)(void *addr);
typedef void (*KmCreateMappingLate)(phys_addr_t phys, unsigned long virt, phys_addr_t size, pgprot_t prot);

int kw_interlock_write_u32(const void *addr, u32 value_orig, u32 value_new) {
	int atomic_res;
	// I failed to make cmpxchg() work, this is my hack
	memcpy((void*)addr, &value_new, 4);
	flush_icache_range(((unsigned long)addr) &~ 0xfff, (((unsigned long)addr) &~ 0xfff) + 0x1000);
	memcpy(&atomic_res, addr, 4);
	if (atomic_res == value_orig) {
		printk("write error: no change\n");
	} else if (atomic_res == value_new) {
		// matches new value, success
		return 0;
	} else {
		printk("write error: result was unexpected\n");
	}
	return -1;
}

int kw_write(const void *dst, const void *src, size_t size) {
	size_t i = 0;
	u32 *dst_ptr = (u32*)dst;
	u32 *src_ptr = (u32*)src;
	u32 value_orig = 0;
	u32 value_new = 0;
	if (0 != (size % 4)) {
		printk("write is not a multple of 4");
		return -1;
	}
	for (i = 0; i < size; i += 4) {
		memcpy(&value_orig, dst_ptr, sizeof(value_orig));
		memcpy(&value_new, src_ptr, sizeof(value_new));
		if (kw_interlock_write_u32((const void *)dst_ptr, value_orig, value_new)) {
			printk("Failed to write\n");
			return -1;
		}
		dst_ptr++;
		src_ptr++;
	}
	return 0;
}

/*------------------------------------------------------------------*/
// DANGERZONE
typedef unsigned long (*DzKAllSymLookupName)(const char *name);
typedef int (*DzQseecomScmCallFn)(u32 svc_id, u32 tz_cmd_id, const void *cmd_buf, size_t cmd_len, void *resp_buf, size_t resp_len);
typedef bool (*DzIsScmArmV8Fn)(void);
typedef int (*DzQseecomScmCall2Fn)(u32 svc_id, u32 tz_cmd_id, const void *req_buf, void *resp_buf);
typedef int (*DzScmCallFn)(u32 svc_id, u32 tz_cmd_id, const void *cmd_buf, size_t cmd_len, void *resp_buf, size_t resp_len);
typedef int (*DzQseecomScmCallHookFn)(void *ctx, DzQseecomScmCallFn orig, u32 svc_id, u32 tz_cmd_id, const void *cmd_buf, size_t cmd_len, void *resp_buf, size_t resp_len);

#define DZ_QSEE_VERSION_00			0x400000
#define DZ_QSEE_VERSION_01			0x401000
#define DZ_QSEE_VERSION_02			0x402000
#define DZ_QSEE_VERSION_03			0x403000
#define DZ_QSEE_VERSION_04			0x404000
#define DZ_QSEE_VERSION_05			0x405000
#define DZ_QSEE_VERSION_20			0x800000
#define DZ_QSEE_VERSION_40			0x1000000

#define DZ_SCM_SVC_BOOT			0x1
#define DZ_SCM_SVC_PIL			0x2
#define DZ_SCM_SVC_UTIL			0x3
#define DZ_SCM_SVC_TZ			0x4
#define DZ_SCM_SVC_IO			0x5
#define DZ_SCM_SVC_INFO			0x6
#define DZ_SCM_SVC_SSD			0x7
#define DZ_SCM_SVC_FUSE			0x8
#define DZ_SCM_SVC_PWR			0x9
#define DZ_SCM_SVC_MP			0xC
#define DZ_SCM_SVC_DCVS			0xD
#define DZ_SCM_SVC_ES			0x10
#define DZ_SCM_SVC_HDCP			0x11
#define DZ_SCM_SVC_MDTP			0x12
#define DZ_SCM_SVC_LMH			0x13
#define DZ_SCM_SVC_SMMU_PROGRAM		0x15
#define DZ_SCM_SVC_QDSS			0x16
#define DZ_SCM_SVC_TZSCHEDULER		0xFC
#define DZ_SCM_SVC_BW			0xFD

/* Save partition image hash for authentication check */
#define	DZ_SCM_SAVE_PARTITION_HASH_ID	0x01

/* Check if enterprise security is activate */
#define	DZ_SCM_IS_ACTIVATED_ID			0x02

/* Encrypt/Decrypt Data Integrity Partition (DIP) for MDTP */
#define DZ_SCM_MDTP_CIPHER_DIP			0x01

#define DZ_SCM_QSEOS_CMD				0x01
#define DZ_SCM_INFO_QSEE_VERSION 		0x03


#define DZ_MAX_APP_NAME_SIZE  64

#define DZ_QSEECOM_KEY_ID_SIZE   32

#define DZ_QSEOS_RESULT_FAIL_SEND_CMD_NO_THREAD  -19   /*0xFFFFFFED*/
#define DZ_QSEOS_RESULT_FAIL_UNSUPPORTED_CE_PIPE -63
#define DZ_QSEOS_RESULT_FAIL_KS_OP               -64
#define DZ_QSEOS_RESULT_FAIL_KEY_ID_EXISTS       -65
#define DZ_QSEOS_RESULT_FAIL_MAX_KEYS            -66
#define DZ_QSEOS_RESULT_FAIL_SAVE_KS             -67
#define DZ_QSEOS_RESULT_FAIL_LOAD_KS             -68
#define DZ_QSEOS_RESULT_FAIL_KS_ALREADY_DONE     -69
#define DZ_QSEOS_RESULT_FAIL_KEY_ID_DNE          -70
#define DZ_QSEOS_RESULT_FAIL_INCORRECT_PSWD      -71
#define DZ_QSEOS_RESULT_FAIL_MAX_ATTEMPT         -72
#define DZ_QSEOS_RESULT_FAIL_PENDING_OPERATION   -73

enum dz_qseecom_command_scm_resp_type {
	DZ_QSEOS_APP_ID = 0xEE01,
	DZ_QSEOS_LISTENER_ID
};

enum dz_qseecom_qceos_cmd_id {
	DZ_QSEOS_APP_START_COMMAND      = 0x01,
	DZ_QSEOS_APP_SHUTDOWN_COMMAND,
	DZ_QSEOS_APP_LOOKUP_COMMAND,
	DZ_QSEOS_REGISTER_LISTENER,
	DZ_QSEOS_DEREGISTER_LISTENER,
	DZ_QSEOS_CLIENT_SEND_DATA_COMMAND,
	DZ_QSEOS_LISTENER_DATA_RSP_COMMAND,
	DZ_QSEOS_LOAD_EXTERNAL_ELF_COMMAND,
	DZ_QSEOS_UNLOAD_EXTERNAL_ELF_COMMAND,
	DZ_QSEOS_GET_APP_STATE_COMMAND,
	DZ_QSEOS_LOAD_SERV_IMAGE_COMMAND,
	DZ_QSEOS_UNLOAD_SERV_IMAGE_COMMAND,
	DZ_QSEOS_APP_REGION_NOTIFICATION,
	DZ_QSEOS_REGISTER_LOG_BUF_COMMAND,
	DZ_QSEOS_RPMB_PROVISION_KEY_COMMAND,
	DZ_QSEOS_RPMB_ERASE_COMMAND,
	DZ_QSEOS_GENERATE_KEY  = 0x11,
	DZ_QSEOS_DELETE_KEY,
	DZ_QSEOS_MAX_KEY_COUNT,
	DZ_QSEOS_SET_KEY,
	DZ_QSEOS_UPDATE_KEY_USERINFO,
	DZ_QSEOS_TEE_OPEN_SESSION,
	DZ_QSEOS_TEE_INVOKE_COMMAND,
	DZ_QSEOS_TEE_INVOKE_MODFD_COMMAND = DZ_QSEOS_TEE_INVOKE_COMMAND,
	DZ_QSEOS_TEE_CLOSE_SESSION,
	DZ_QSEOS_TEE_REQUEST_CANCELLATION,
	DZ_QSEOS_CONTINUE_BLOCKED_REQ_COMMAND,
	DZ_QSEOS_RPMB_CHECK_PROV_STATUS_COMMAND = 0x1B,
	DZ_QSEOS_CLIENT_SEND_DATA_COMMAND_WHITELIST = 0x1C,
	DZ_QSEOS_TEE_OPEN_SESSION_WHITELIST = 0x1D,
	DZ_QSEOS_TEE_INVOKE_COMMAND_WHITELIST = 0x1E,
	DZ_QSEOS_LISTENER_DATA_RSP_COMMAND_WHITELIST = 0x1F,
	DZ_QSEOS_FSM_LTEOTA_REQ_CMD = 0x109,
	DZ_QSEOS_FSM_LTEOTA_REQ_RSP_CMD = 0x110,
	DZ_QSEOS_FSM_IKE_REQ_CMD = 0x203,
	DZ_QSEOS_FSM_IKE_REQ_RSP_CMD = 0x204,
	DZ_QSEOS_FSM_OEM_FUSE_WRITE_ROW = 0x301,
	DZ_QSEOS_FSM_OEM_FUSE_READ_ROW = 0x302,
	DZ_QSEOS_FSM_ENCFS_REQ_CMD = 0x403,
	DZ_QSEOS_FSM_ENCFS_REQ_RSP_CMD = 0x404,

	DZ_QSEOS_CMD_MAX     = 0xEFFFFFFF,

	DZ_QSEOS_CMD_INVALID = 0xffffffff
};

enum dz_qseecom_qceos_cmd_status {
	DZ_QSEOS_RESULT_SUCCESS = 0,
	DZ_QSEOS_RESULT_INCOMPLETE,
	DZ_QSEOS_RESULT_BLOCKED_ON_LISTENER,
	DZ_QSEOS_RESULT_FAILURE  = 0xFFFFFFFF
};

__packed struct dz_qseecom_command_scm_resp {
	uint32_t result;
	enum dz_qseecom_command_scm_resp_type resp_type;
	unsigned int data;
};

__packed struct dz_qseecom_check_app_ireq {
	uint32_t qsee_cmd_id;
	char     app_name[DZ_MAX_APP_NAME_SIZE];
};

__packed struct dz_qseecom_unload_app_ireq {
	uint32_t qsee_cmd_id;
	uint32_t app_id;
};

__packed struct dz_qseecom_load_app_ireq {
	uint32_t qsee_cmd_id;
	uint32_t mdt_len;		/* Length of the mdt file */
	uint32_t img_len;		/* Length of .bxx and .mdt files */
	uint32_t phy_addr;		/* phy addr of the start of image */
	char     app_name[DZ_MAX_APP_NAME_SIZE];	/* application name*/
};

__packed struct dz_qseecom_load_app_64bit_ireq {
	uint32_t qsee_cmd_id;
	uint32_t mdt_len;
	uint32_t img_len;
	uint64_t phy_addr;
	char     app_name[DZ_MAX_APP_NAME_SIZE];
};

struct dz_qseecom_registered_app_list {
	struct list_head                 list;
	u32  app_id;
	u32  ref_cnt;
	char app_name[DZ_MAX_APP_NAME_SIZE];
	u32  app_arch;
	bool app_blocked;
	u32  blocked_on_listener_id;
};

struct dz_qseecom_control {
	struct ion_client *ion_clnt;		/* Ion client */
	struct list_head  registered_listener_list_head;
	spinlock_t        registered_listener_list_lock;

	struct list_head  registered_app_list_head;
	spinlock_t        registered_app_list_lock;

	struct list_head   registered_kclient_list_head;
	spinlock_t        registered_kclient_list_lock;

	wait_queue_head_t send_resp_wq;
	int               send_resp_flag;

	uint32_t          qseos_version;
	uint32_t          qsee_version;

	// There are more fields, I've left them out for now
};

void *dz_mine_qseecom_unregister_listener(void *kallsyms_lookup_name) {
    DzKAllSymLookupName lookup = (DzKAllSymLookupName)kallsyms_lookup_name;
    return (void*)(lookup("qseecom_unregister_listener"));
}

void *dz_mine_qseecom_scm_call(void *qseecom_unregister_listener) {
    return aarch64le_mine_bl_addr(qseecom_unregister_listener, 0x100, 0);
}

const char *dz_svc_id_name(u32 svc_id) {
	switch(svc_id) {
		case DZ_SCM_SVC_BOOT:
			return "SCM_SVC_BOOT";
		case DZ_SCM_SVC_PIL:
			return "SCM_SVC_PIL";
		case DZ_SCM_SVC_UTIL:
			return "SCM_SVC_UTIL";
		case DZ_SCM_SVC_TZ:
			return "SCM_SVC_TZ";
		case DZ_SCM_SVC_IO:
			return "SCM_SVC_IO";
		case DZ_SCM_SVC_INFO:
			return "SCM_SVC_INFO";
		case DZ_SCM_SVC_SSD:
			return "SCM_SVC_SSD";
		case DZ_SCM_SVC_FUSE:
			return "SCM_SVC_FUSE";
		case DZ_SCM_SVC_PWR:
			return "SCM_SVC_PWR";
		case DZ_SCM_SVC_MP:
			return "SCM_SVC_MP";
		case DZ_SCM_SVC_DCVS:
			return "SCM_SVC_DCVS";
		case DZ_SCM_SVC_ES:
			return "SCM_SVC_ES";
		case DZ_SCM_SVC_HDCP:
			return "SCM_SVC_HDCP";
		case DZ_SCM_SVC_MDTP:
			return "SCM_SVC_MDTP";
		case DZ_SCM_SVC_LMH:
			return "SCM_SVC_LMH";
		case DZ_SCM_SVC_SMMU_PROGRAM:
			return "SCM_SVC_SMMU_PROGRAM";
		case DZ_SCM_SVC_QDSS:
			return "SCM_SVC_QDSS";
		case DZ_SCM_SVC_TZSCHEDULER:
			return "SCM_SVC_TZSCHEDULER";
		case DZ_SCM_SVC_BW:
			return "SCM_SVC_BW";
		default:
			break;
	}
	return "";
}

const char *dz_scm_cmd_name(u32 svc_id, u32 scm_cmd_id) {
	if (DZ_SCM_SVC_INFO == svc_id) {
		switch(scm_cmd_id) {
			case DZ_SCM_INFO_QSEE_VERSION:
				return "SCM_INFO_QSEE_VERSION";
			default:
			break;
		}
	}
	if (DZ_SCM_SVC_ES == svc_id) {
		switch(scm_cmd_id) {
			case DZ_SCM_SAVE_PARTITION_HASH_ID:
				return "SCM_SAVE_PARTITION_HASH_ID";
			default:
			break;
		}
	}
	if (DZ_SCM_SVC_TZSCHEDULER == svc_id) {
		switch(scm_cmd_id) {
			case DZ_SCM_QSEOS_CMD:
				return "SCM_QSEOS_CMD";
			default:
				break;
		}
	}
	return "";
}

const char *dz_svm_resp_name(u32 resp_type) {
	switch(resp_type) {
		case DZ_QSEOS_APP_ID:
			return "QSEOS_APP_ID";
		case DZ_QSEOS_LISTENER_ID:
			return "QSEOS_LISTENER_ID";
		default:
			break;
	}
	return "";
}

u32 dz_qseos_cmd_id(u32 svc_id, u32 scm_cmd_id, const void *req_buf) {
	u32 qseos_cmd_id = (u32)DZ_QSEOS_CMD_INVALID;
	if (DZ_SCM_SVC_TZSCHEDULER != svc_id) {
		return qseos_cmd_id;
	}
	if (DZ_SCM_QSEOS_CMD != scm_cmd_id) {
		return qseos_cmd_id;
	}
	return *(u32 *)req_buf;
}

const char *dz_qseos_cmd_name(u32 qseos_cmd_id) {
	switch(qseos_cmd_id) {
		case DZ_QSEOS_APP_START_COMMAND:
			return "QSEOS_APP_START_COMMAND";
		case DZ_QSEOS_APP_SHUTDOWN_COMMAND:
			return "QSEOS_APP_SHUTDOWN_COMMAND";
		case DZ_QSEOS_APP_LOOKUP_COMMAND:
			return "QSEOS_APP_LOOKUP_COMMAND";
		case DZ_QSEOS_REGISTER_LISTENER:
			return "QSEOS_REGISTER_LISTENER";
		case DZ_QSEOS_DEREGISTER_LISTENER:
			return "QSEOS_DEREGISTER_LISTENER";
		case DZ_QSEOS_CLIENT_SEND_DATA_COMMAND:
			return "QSEOS_CLIENT_SEND_DATA_COMMAND";
		case DZ_QSEOS_LISTENER_DATA_RSP_COMMAND:
			return "QSEOS_LISTENER_DATA_RSP_COMMAND";
		case DZ_QSEOS_LOAD_EXTERNAL_ELF_COMMAND:
			return "QSEOS_LOAD_EXTERNAL_ELF_COMMAND";
		case DZ_QSEOS_UNLOAD_EXTERNAL_ELF_COMMAND:
			return "QSEOS_UNLOAD_EXTERNAL_ELF_COMMAND";
		case DZ_QSEOS_GET_APP_STATE_COMMAND:
			return "QSEOS_GET_APP_STATE_COMMAND";
		case DZ_QSEOS_LOAD_SERV_IMAGE_COMMAND:
			return "QSEOS_LOAD_SERV_IMAGE_COMMAND";
		case DZ_QSEOS_UNLOAD_SERV_IMAGE_COMMAND:
			return "QSEOS_UNLOAD_SERV_IMAGE_COMMAND";
		case DZ_QSEOS_APP_REGION_NOTIFICATION:
			return "QSEOS_APP_REGION_NOTIFICATION";
		case DZ_QSEOS_REGISTER_LOG_BUF_COMMAND:
			return "QSEOS_REGISTER_LOG_BUF_COMMAND";
		case DZ_QSEOS_RPMB_PROVISION_KEY_COMMAND:
			return "QSEOS_RPMB_PROVISION_KEY_COMMAND";
		case DZ_QSEOS_RPMB_ERASE_COMMAND:
			return "QSEOS_RPMB_ERASE_COMMAND";
		case DZ_QSEOS_GENERATE_KEY:
			return "QSEOS_GENERATE_KEY";
		case DZ_QSEOS_DELETE_KEY:
			return "QSEOS_DELETE_KEY";
		case DZ_QSEOS_MAX_KEY_COUNT:
			return "QSEOS_MAX_KEY_COUNT";
		case DZ_QSEOS_SET_KEY:
			return "QSEOS_SET_KEY";
		case DZ_QSEOS_UPDATE_KEY_USERINFO:
			return "QSEOS_UPDATE_KEY_USERINFO";
		case DZ_QSEOS_TEE_OPEN_SESSION:
			return "QSEOS_TEE_OPEN_SESSION";
		case DZ_QSEOS_TEE_INVOKE_COMMAND:
			return "QSEOS_TEE_INVOKE_COMMAND";
		case DZ_QSEOS_TEE_CLOSE_SESSION:
			return "QSEOS_TEE_CLOSE_SESSION";
		case DZ_QSEOS_TEE_REQUEST_CANCELLATION:
			return "QSEOS_TEE_REQUEST_CANCELLATION";
		case DZ_QSEOS_CONTINUE_BLOCKED_REQ_COMMAND:
			return "QSEOS_CONTINUE_BLOCKED_REQ_COMMAND";
		case DZ_QSEOS_RPMB_CHECK_PROV_STATUS_COMMAND:
			return "QSEOS_RPMB_CHECK_PROV_STATUS_COMMAND";
		case DZ_QSEOS_CLIENT_SEND_DATA_COMMAND_WHITELIST:
			return "QSEOS_CLIENT_SEND_DATA_COMMAND_WHITELIST";
		case DZ_QSEOS_TEE_OPEN_SESSION_WHITELIST:
			return "QSEOS_TEE_OPEN_SESSION_WHITELIST";
		case DZ_QSEOS_TEE_INVOKE_COMMAND_WHITELIST:
			return "QSEOS_TEE_INVOKE_COMMAND_WHITELIST";
		case DZ_QSEOS_LISTENER_DATA_RSP_COMMAND_WHITELIST:
			return "QSEOS_LISTENER_DATA_RSP_COMMAND_WHITELIST";
		case DZ_QSEOS_FSM_LTEOTA_REQ_CMD:
			return "QSEOS_FSM_LTEOTA_REQ_CMD";
		case DZ_QSEOS_FSM_LTEOTA_REQ_RSP_CMD:
			return "QSEOS_FSM_LTEOTA_REQ_RSP_CMD";
		case DZ_QSEOS_FSM_IKE_REQ_CMD:
			return "QSEOS_FSM_IKE_REQ_CMD";
		case DZ_QSEOS_FSM_IKE_REQ_RSP_CMD:
			return "QSEOS_FSM_IKE_REQ_RSP_CMD";
		case DZ_QSEOS_FSM_OEM_FUSE_WRITE_ROW:
			return "QSEOS_FSM_OEM_FUSE_WRITE_ROW";
		case DZ_QSEOS_FSM_OEM_FUSE_READ_ROW:
			return "QSEOS_FSM_OEM_FUSE_READ_ROW";
		case DZ_QSEOS_FSM_ENCFS_REQ_CMD:
			return "QSEOS_FSM_ENCFS_REQ_CMD";
		case DZ_QSEOS_FSM_ENCFS_REQ_RSP_CMD:
			return "QSEOS_FSM_ENCFS_REQ_RSP_CMD";
		default:
			break;
	}
	return "";
}

void *dz_mine_qseecom_control(void *qseecom_probe, u32 qsee_version) {
	// qseecom_probe does a lot of setup of the structure we need
	// so we will mine it for the ADRP instruction
	// then we will mine the ADRP page for the version numbers (that we already know - thats 8 predictable bytes)
	size_t index;
	size_t byte_index;
	void *page_addr;
	void *qsee_version_addr = NULL;
	struct dz_qseecom_control* qseecom = NULL;
	for (index = 0; index < 4; index++) {
		page_addr = aarch64le_mine_adrp_addr(qseecom_probe, 0x100, index);
		if (page_addr) {
			for (byte_index = 4; byte_index < 0x1000; byte_index += 4) {
				if ((*(((u32*)page_addr) + (byte_index/4)) == qsee_version) && ((*(((u32*)page_addr) + (byte_index/4) - 1) < 0x100))) {
					qsee_version_addr = (void*)(((u32*)page_addr) + (byte_index/4));
					break;
				}
			}
		}
	}
	// work back from the version address to find the qseecom structure pointer we need.
	// then check the version numbers one more time
	if (qsee_version_addr) {
		qseecom = NULL;
		qseecom = (struct dz_qseecom_control*)((unsigned long)qsee_version_addr - (unsigned long)(&(qseecom->qsee_version)));
		if ((qseecom->qsee_version == qsee_version) && (qseecom->qseos_version < 0x100)) {
			return (void*)qseecom;
		}
	}
	return NULL;
}

void *dz_mine_create_mapping_late(void *mark_rodata_ro) {
	// there should be 2 calls to this function:
	// 1) for ROX
	// 2) for RO
	// see in your kernel arch/arm64/mm/mmc.c
	void *addr1 = aarch64le_mine_bl_addr(mark_rodata_ro, 0x100, 0);
	void *addr2 = aarch64le_mine_bl_addr(mark_rodata_ro, 0x100, 1);
	if (addr1 != addr2) {
		// unexpected, has the kernel changed since I made this
		return NULL;
	}
	return addr1;
}

void dz_dump_apps(void *qseecom) {
	unsigned long flags;
	struct dz_qseecom_registered_app_list *ptr_app;
	if (!qseecom) {
		return;
	}
	spin_lock_irqsave(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	list_for_each_entry(ptr_app, &(((struct dz_qseecom_control*)qseecom)->registered_app_list_head),
						list) {
		printk("app_id=0x%08x, app_name=%s\n", ptr_app->app_id, ptr_app->app_name);
	}
	spin_unlock_irqrestore(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
}

void dz_app_name(void *qseecom, u32 app_id, char *app_name) {
	unsigned long flags;
	struct dz_qseecom_registered_app_list *ptr_app;
	memset(app_name, 0, DZ_MAX_APP_NAME_SIZE);
	if (!qseecom) {
		return;
	}
	spin_lock_irqsave(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	list_for_each_entry(ptr_app, &(((struct dz_qseecom_control*)qseecom)->registered_app_list_head),
						list) {
		if (ptr_app->app_id == app_id) {
			memcpy(app_name, ptr_app->app_name, DZ_MAX_APP_NAME_SIZE);
		}
	}
	spin_unlock_irqrestore(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
}

u32 dz_app_id(void *qseecom, const char *app_name) {
	unsigned long flags;
	struct dz_qseecom_registered_app_list *ptr_app;
	u32 app_id = 0;
	if (!qseecom) {
		return 0;
	}
	spin_lock_irqsave(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	list_for_each_entry(ptr_app, &(((struct dz_qseecom_control*)qseecom)->registered_app_list_head),
						list) {
		if (0 == strcmp(app_name, ptr_app->app_name)) {
			app_id = ptr_app->app_id;
		}
	}
	spin_unlock_irqrestore(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	return app_id;
}

void *dz_mine_is_scm_armv8(void *qseecom_scm_call) {
    return aarch64le_mine_bl_addr(qseecom_scm_call, 0x30, 0);
}

uint32_t dz_get_qsee_version(void *qseecom_scm_call) {
	int rc = -EINVAL;
	uint32_t feature = 10;
	struct dz_qseecom_command_scm_resp resp;
	uint32_t qsee_version;
	if (NULL == qseecom_scm_call) {
		return 0;
	}
	rc = ((DzQseecomScmCallFn)qseecom_scm_call)(DZ_SCM_SVC_INFO, DZ_SCM_INFO_QSEE_VERSION, &feature, sizeof(feature), &resp, sizeof(resp));
	if (rc) {
		return 0;
	}
	qsee_version = resp.result;
	return qsee_version;
}

uint32_t dz_get_qsee_version2(void *qseecom_scm_call2) {
	int rc = -EINVAL;
	uint32_t feature = 10;
	struct dz_qseecom_command_scm_resp resp;
	uint32_t qsee_version;
	if (NULL == qseecom_scm_call2) {
		return 0;
	}
	rc = ((DzQseecomScmCall2Fn)qseecom_scm_call2)(DZ_SCM_SVC_INFO, DZ_SCM_INFO_QSEE_VERSION, &feature, &resp);
	if (rc) {
		return 0;
	}
	qsee_version = resp.result;
	return qsee_version;
}

uint32_t dz_get_qsee_app_id(void *qseecom_scm_call, const char *app_name) {
	uint32_t app_id = 0;
	int rc = 0;
	struct dz_qseecom_check_app_ireq req;
	struct dz_qseecom_command_scm_resp resp;
	if (NULL == qseecom_scm_call) {
		return 0;
	}
	if (NULL == app_name) {
		return 0;
	}
	req.qsee_cmd_id = DZ_QSEOS_APP_LOOKUP_COMMAND;
	strlcpy(req.app_name, app_name, DZ_MAX_APP_NAME_SIZE);
	req.app_name[DZ_MAX_APP_NAME_SIZE-1] = '\0';
	memset((void *)&resp, 0, sizeof(resp)); // you must clear memory for this command
	rc = ((DzQseecomScmCallFn)qseecom_scm_call)(DZ_SCM_SVC_TZSCHEDULER, DZ_SCM_QSEOS_CMD, &req, sizeof(req), &resp, sizeof(resp));
	if (rc) {
		return 0;
	}
	if (resp.result == DZ_QSEOS_RESULT_FAILURE) {
		return 0;
	}
	if (resp.resp_type == DZ_QSEOS_APP_ID) {
		app_id = resp.data;
	}
	return app_id;
}

int dz_ion_buf_alloc(void *qseecom, uint32_t size, struct ion_handle **pihandle, u8 **data, ion_phys_addr_t *paddr)
{
	size_t len = 0;
	int ret = 0;
	ion_phys_addr_t pa;
	struct ion_handle *ihandle = NULL;
	u8 *img_data = NULL;

	if (NULL == qseecom) {
		return -EINVAL;
	}

	ihandle = ion_alloc(((struct dz_qseecom_control*)qseecom)->ion_clnt, size, SZ_4K, ION_HEAP(ION_QSECOM_HEAP_ID), 0);

	if (IS_ERR_OR_NULL(ihandle)) {
		printk("DZ ION alloc failed\n");
		return -ENOMEM;
	}
	img_data = (u8 *)ion_map_kernel(((struct dz_qseecom_control*)qseecom)->ion_clnt, ihandle);

	if (IS_ERR_OR_NULL(img_data)) {
		printk("DZ ION memory mapping for image loading failed\n");
		ret = -ENOMEM;
		goto exit_ion_free;
	}
	/* Get the physical address of the ION BUF */
	ret = ion_phys(((struct dz_qseecom_control*)qseecom)->ion_clnt, ihandle, &pa, &len);
	if (ret) {
		printk("DZ physical memory retrieval failure\n");
		ret = -EIO;
		goto exit_ion_unmap_kernel;
	}

	*pihandle = ihandle;
	*data = img_data;
	*paddr = pa;
	return ret;

exit_ion_unmap_kernel:
	ion_unmap_kernel(((struct dz_qseecom_control*)qseecom)->ion_clnt, ihandle);
exit_ion_free:
	ion_free(((struct dz_qseecom_control*)qseecom)->ion_clnt, ihandle);
	ihandle = NULL;
	return ret;
}

void dz_ion_buf_free(void *qseecom, struct ion_handle **ihandle)
{
	if (NULL == qseecom) {
		return;
	}
	ion_unmap_kernel(((struct dz_qseecom_control*)qseecom)->ion_clnt, *ihandle);
	ion_free(((struct dz_qseecom_control*)qseecom)->ion_clnt, *ihandle);
	*ihandle = NULL;
}


int dz_unload_app(void *qseecom, void *qseecom_scm_call, u32 app_id) {
	int ret = -EFAULT;
	struct dz_qseecom_unload_app_ireq req;
	struct dz_qseecom_command_scm_resp resp;
	unsigned long flags;
	struct dz_qseecom_registered_app_list *ptr_app;
	struct dz_qseecom_registered_app_list *del_app;
	if (NULL == qseecom) {
		return ret;
	}
	if (NULL == qseecom_scm_call) {
		return ret;
	}
	req.qsee_cmd_id = DZ_QSEOS_APP_SHUTDOWN_COMMAND;
	req.app_id = app_id;
	ret = ((DzQseecomScmCallFn)qseecom_scm_call)(DZ_SCM_SVC_TZSCHEDULER, DZ_SCM_QSEOS_CMD, &req, sizeof(req), &resp, sizeof(resp));
	if (0 == ret) {
		spin_lock_irqsave(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
		del_app = NULL;
		list_for_each_entry(ptr_app, &(((struct dz_qseecom_control*)qseecom)->registered_app_list_head),
							list) {
			if (ptr_app->app_id == app_id) {
				del_app = ptr_app;
			}
		}
		if (del_app)  {
			list_del(&del_app->list);
			kzfree(del_app);
		}
		spin_unlock_irqrestore(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	}
	return ret;
}

size_t dz_file_size(const char *path) {
	struct file *f;
	size_t size;
	f = file_open(path, O_RDONLY, 0);
    if (NULL != f) {
		size = (size_t)file_lseek(f, 0, SEEK_END);
		if ((size > 0) && (size < ((size_t)1024 * (size_t)1024 * (size_t)1024))) {
			file_close(f);
			return size;
		}
		file_close(f);
	}
	return 0;
}

int dz_file_read(u8 *buf, size_t buf_size, const char *path) {
	struct file *f;
	int offset = 0;
	int recd = 0;
	f = file_open(path, O_RDONLY, 0);
    if (NULL != f) {
		while (buf_size > 0) {
			recd = file_read(f, offset, buf, buf_size);
			if (recd <= 0) {
				printk("DZ read error (or no bytes) %d\n", recd);
				file_close(f);
				return -EFAULT;
			}
			if (recd > buf_size) {
				printk("DZ read was larger than buffer\n");
				file_close(f);
				return -EFAULT;
			}
			printk("DZ read %d bytes\n", recd);
			buf += recd;
			buf_size -= recd;
			offset += recd;
		}
		file_close(f);
	}
	return 0;
}

size_t dz_mdt_img_size(const char *mdt, size_t *mdt_size, size_t *img_size) {
	char path[512];
	char *pos;
	size_t size = 0;
	size_t total_size = 0;
	int index = 0;

	// size of mdt
	size = dz_file_size(mdt);
	if (0 == size) {
		return 0;
	}
	printk("Size %s %d\n", mdt, (int)size);
	*mdt_size = size;
	total_size += size;

	// size of each binary image file
	strlcpy(path, mdt, sizeof(path));
	path[sizeof(path)-1] = '\0';
	pos = strstr(path, ".mdt");
	if (NULL == pos) {
		return 0;
	}
	while (size > 0) {
		sprintf(pos, ".b%02d", index);
		size = dz_file_size(path);
		if (0 == size) {
			break;
		}
		printk("Size %s %d\n", path, (int)size);
		total_size += size;
		index++;
		if (index > 50) {
			printk("DZ More than 50 files read, thats just crazy\n");
			return 0;
		}
	}
	if (0 == index) {
		printk("No binary files found for mdt file\n");
		return 0;
	}
	printk("DZ total size %d\n", (int)total_size);
	*img_size = total_size - (*mdt_size);
	return total_size;
}

int dz_mdt_img_read(u8 *buf, size_t buf_size, const char *mdt) {
	char path[512];
	char *pos;
	size_t size = 0;
	int index = 0;

	// read mdt file
	size = dz_file_size(mdt);
	if (0 == size) {
		return -EFAULT;
	}
	if (size > buf_size) {
		return -EFAULT;
	}
	if (dz_file_read(buf, size, mdt)) {
		return -EFAULT;
	}
	printk("Read %s %d\n", mdt, (int)size);
	buf_size -= size;
	buf += size;
	
	// read each binary file
	strlcpy(path, mdt, sizeof(path));
	path[sizeof(path)-1] = '\0';
	pos = strstr(path, ".mdt");
	if (NULL == pos) {
		return 0;
	}
	while (size > 0) {
		sprintf(pos, ".b%02d", index);
		size = dz_file_size(path);
		if (0 == size) {
			break;
		}
		if (size > buf_size) {
			return -EFAULT;
		}
		if (dz_file_read(buf, size, path)) {
			return -EFAULT;
		}
		printk("Read %s %d\n", path, (int)size);
		buf_size -= size;
		buf += size;
		index++;
		if (index > 50) {
			printk("DZ More than 50 files read, thats just crazy\n");
			return -EFAULT;
		}
	}
	if (0 == index) {
		printk("DZ No binary files found for mdt file\n");
		return -EFAULT;
	}
	if (0 != buf_size) {
		printk("DZ not enough bytes read\n");
		return -EFAULT;
	}
	return 0;
}

int dz_load_app(void *qseecom, void *qseecom_scm_call, const char *app_name, const char *path) {
	int ret = -EFAULT;
	struct ion_handle *ihandle = NULL;
	size_t size = 0;
	size_t mdt_size = 0;
	size_t img_size = 0;
	u8 *data = NULL;
	ion_phys_addr_t pa = 0;
	struct dz_qseecom_load_app_ireq load_req;
	struct dz_qseecom_load_app_64bit_ireq load_req_64bit;
	struct dz_qseecom_command_scm_resp resp;
	void *cmd_buf = NULL;
	size_t cmd_len;
	struct dz_qseecom_registered_app_list *entry = NULL;
	unsigned long flags = 0;
	u32 app_arch = 0;
	if (NULL == qseecom) {
		return ret;
	}
	if (NULL == qseecom_scm_call) {
		return ret;
	}
	if (NULL == app_name) {
		return ret;
	}
	if (NULL == path) {
		return ret;
	}
	if (NULL == strstr(path, ".mdt")) {
		printk("DZ only .mdt files are supported\n");
		return ret;
	}
	size = dz_mdt_img_size(path, &mdt_size, &img_size);
	if (0 == size) {
		printk("DZ failed to get mdt/img size\n");
		return ret;
	}
	printk("DZ image size (mdt + binaries) is %d bytes\n", (int)size);
	if (dz_ion_buf_alloc(qseecom, size, &ihandle, &data, &pa)) {
		printk("DZ failed ION allocate\n");
		return ret;
	}
	printk("DZ ION buffer %d (mdt=%d,img=%d)", (int)size, (int)mdt_size, (int)img_size);
	if (dz_mdt_img_read(data, size, path)) {
		printk("DZ failed mdt/img read\n");
		dz_ion_buf_free(qseecom, &ihandle);
		return ret;
	}
	ret = msm_ion_do_cache_op(((struct dz_qseecom_control*)qseecom)->ion_clnt, ihandle, NULL, size, ION_IOC_CLEAN_INV_CACHES);
	if (ret) {
		printk("DZ cache operation failed\n");
		dz_ion_buf_free(qseecom, &ihandle);
		return ret;
	}
	printk("DZ ION populated with mdt/img %d bytes\n", (int)size);
	if (0 != memcmp(data + 1, "ELF", 3)) {
		printk("DZ ION image doesn't look like an ELF\n");
		dz_ion_buf_free(qseecom, &ihandle);
		return ret;
	}
	if (1 == data[4]) {
		printk("DZ 32-bit image\n");
		app_arch = 0;
	} else if (2 == data[4]) {
		printk("DZ 64-bit image\n");
		app_arch = 1;
	} else {
		printk("DZ ION ELF architecture 0x%02x is unknown\n", data[4]);
		dz_ion_buf_free(qseecom, &ihandle);
		return ret;
	}
	if (((struct dz_qseecom_control*)qseecom)->qsee_version < DZ_QSEE_VERSION_40) {
		memset(&load_req, 0, sizeof(load_req));
		load_req.qsee_cmd_id = DZ_QSEOS_APP_START_COMMAND;
		load_req.mdt_len = (uint32_t)mdt_size;
		load_req.img_len = (uint32_t)img_size;
		strlcpy(load_req.app_name, app_name, DZ_MAX_APP_NAME_SIZE);
		load_req.phy_addr = (uint32_t)pa;
		cmd_buf = (void *)&load_req;
		cmd_len = sizeof(struct dz_qseecom_load_app_ireq);
	} else {
		memset(&load_req_64bit, 0, sizeof(load_req_64bit));
		load_req_64bit.qsee_cmd_id = DZ_QSEOS_APP_START_COMMAND;
		load_req_64bit.mdt_len = (uint32_t)mdt_size;
		load_req_64bit.img_len = (uint32_t)img_size;
		strlcpy(load_req_64bit.app_name, app_name, DZ_MAX_APP_NAME_SIZE);
		load_req_64bit.phy_addr = (uint64_t)pa;
		cmd_buf = (void *)&load_req_64bit;
		cmd_len = sizeof(struct dz_qseecom_load_app_64bit_ireq);
	}
	ret = ((DzQseecomScmCallFn)qseecom_scm_call)(DZ_SCM_SVC_TZSCHEDULER, DZ_SCM_QSEOS_CMD, cmd_buf, cmd_len, &resp, sizeof(resp));
	dz_ion_buf_free(qseecom, &ihandle);
	if (ret) {
		printk("DZ load failed with error %d\n", ret);
		return ret;
	}
	if (resp.result) {
		printk("DZ load failed with result 0x%08x\n", resp.result);
		ret = -EFAULT;
		return ret;
	}
	entry = kmalloc(sizeof(*entry), GFP_KERNEL);
	if (!entry) {
		printk("DZ failed to allocate a list entry for the appx\n");
		ret = -ENOMEM;
		return ret;
	}
	entry->app_id = resp.data;
	strlcpy(entry->app_name, app_name, DZ_MAX_APP_NAME_SIZE);
	entry->ref_cnt = 1;
	entry->app_arch = app_arch;
	entry->app_blocked = false;
	entry->blocked_on_listener_id = 0;
	spin_lock_irqsave(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	list_add_tail(&entry->list, &(((struct dz_qseecom_control*)qseecom)->registered_app_list_head));
	spin_unlock_irqrestore(&(((struct dz_qseecom_control*)qseecom)->registered_app_list_lock), flags);
	printk("DZ app %s loaded with app id 0x%08x\n", app_name, entry->app_id);
	return ret;
}

bool dz_is_scm_armv8(void *is_scm_armv8) {
	if (NULL == is_scm_armv8) {
		return false;
	}
    return ((DzIsScmArmV8Fn)is_scm_armv8)();
}

/*
// NOTES ABOUT THE SCM FUNCTION
ROM:0000000000000000 FD 7B BC A9                             STP             X29, X30, [SP,#-0x40]!
ROM:0000000000000004 FD 03 00 91                             MOV             X29, SP
ROM:0000000000000008 F3 0B 00 F9                             STR             X19, [SP,#0x10]
ROM:000000000000000C F3 03 00 2A                             MOV             W19, W0
ROM:0000000000000010 A1 13 00 F9                             STR             X1, [X29,#0x20]
ROM:0000000000000014 A2 17 00 F9                             STR             X2, [X29,#0x28]
ROM:0000000000000018 A3 1B 00 F9                             STR             X3, [X29,#0x30]
ROM:000000000000001C A4 1F 00 F9                             STR             X4, [X29,#0x38]
ROM:0000000000000020 B2 30 FB 97                             BL              0xFFFFFFFFFFECC2E8
ROM:0000000000000024 00 1C 00 53                             UXTB            W0, W0
ROM:0000000000000028 A4 1F 40 F9                             LDR             X4, [X29,#0x38]
ROM:000000000000002C A3 1B 40 F9                             LDR             X3, [X29,#0x30]
ROM:0000000000000030 A2 17 40 F9                             LDR             X2, [X29,#0x28]
ROM:0000000000000034 A1 13 40 F9                             LDR             X1, [X29,#0x20]
ROM:0000000000000038 E0 00 00 34                             CBZ             W0, loc_54
ROM:000000000000003C E0 03 13 2A                             MOV             W0, W19
ROM:0000000000000040 E3 03 04 AA                             MOV             X3, X4
ROM:0000000000000044 8A F3 FF 97                             BL              0xFFFFFFFFFFFFCE6C   // PATCH THIS WITH A JUMP TO THE STUB
ROM:0000000000000048 F3 0B 40 F9                             LDR             X19, [SP,#0x10]
ROM:000000000000004C FD 7B C4 A8                             LDP             X29, X30, [SP],#0x40
ROM:0000000000000050 C0 03 5F D6                             RET
ROM:0000000000000054                         ; ---------------------------------------------------------------------------
ROM:0000000000000054
ROM:0000000000000054                         loc_54                                  ; CODE XREF: ROM:0000000000000038↑j
ROM:0000000000000054 E0 03 13 2A                             MOV             W0, W19				// START OF STUB GOES HERE (THIS IS AN UNUSED CODE PATH ON ARM64)
ROM:0000000000000058 85 01 80 D2                             MOV             X5, #0xC
ROM:000000000000005C A8 2F FB 97                             BL              0xFFFFFFFFFFECBEFC
ROM:0000000000000060 F3 0B 40 F9                             LDR             X19, [SP,#0x10]
ROM:0000000000000064 FD 7B C4 A8                             LDP             X29, X30, [SP],#0x40
ROM:0000000000000068 C0 03 5F D6                             RET									// END OF STUB GOES HERE (total of 24 bytes)

// NOTES ABOUT THE HOOK CODE
\x89\x00\x00\x58                    ldr x9, #0x10   # ldr x9, =0xBADC0FFEE0DDF00D
\x20\x01\x3f\xd6                    blr x9
\xfb\xff\xff\x17                    b  #xxxxxxxx
\x1f\x20\x03\xd5                    nop
\x0d\xf0\xdd\xe0\xfe\x0f\xdc\xba    =0xBADC0FFEE0DDF00D
*/

typedef struct {
	void *jmp_addr;
	unsigned char jmp_orig[4];
	unsigned char jmp[4];
	void *stub_addr;
	unsigned char stub_orig[24];
	unsigned char stub[24];
	int hooked;
} dz_hook_stub_t;

static DzQseecomScmCall2Fn dz_qseecom_scm_call2_fn = NULL;
static void *dz_qseecom_scm_call_hook_ctx = NULL;
static DzQseecomScmCallHookFn dz_qseecom_scm_call_hook_fn = NULL;
static int dz_qseecom_scm_call1_to_call2(u32 svc_id, u32 tz_cmd_id, const void *cmd_buf, size_t cmd_len, void *resp_buf, size_t resp_len) {
	// NOTE: call2 doesn't use the length arguments
	if (!dz_qseecom_scm_call2_fn) {
		return -1;
	}
	return dz_qseecom_scm_call2_fn(svc_id, tz_cmd_id, cmd_buf, resp_buf);
}
static int dz_qseecom_scm_call2_to_call1(u32 svc_id, u32 tz_cmd_id, const void *req_buf, void *resp_buf) {
	size_t *cmd_len_stack = NULL;
	size_t cmd_len = 0;
	size_t resp_len = 0xC;  // The dissassembly of qseecom_scm_call() shows x5 always being set to 0xc (showing an optimization)
	cmd_len_stack = (size_t*)(&cmd_len_stack);
	cmd_len_stack += 8; // (8 * sizeof(size_t)) bytes up the stack is the cmd_len
	cmd_len = *cmd_len_stack;
	if (cmd_len > 0xffffff) {
		// failed a sanity check
		cmd_len = 0;
	}
	if (!dz_qseecom_scm_call_hook_fn) {
		return -1;
	}
	return dz_qseecom_scm_call_hook_fn(dz_qseecom_scm_call_hook_ctx, dz_qseecom_scm_call1_to_call2, svc_id, tz_cmd_id, req_buf, cmd_len, resp_buf, resp_len);
}

void dz_hook_qseecom_scm_call(dz_hook_stub_t *hook, void *qseecom_scm_call, DzQseecomScmCallHookFn func, void *ctx) {
    void *bl_ins_is_scm_armv8 = NULL;
    void *bl_ins_scm_call = NULL;
    void *ret_ins_scm_call = NULL;
    void *bl_ins_qseecom_scm_call2 = NULL;
    void *ret_ins_qseecom_scm_call2 = NULL;
    void *qseecom_scm_call2 = NULL;
    void *return_addr = NULL;
	size_t free_space = 0;
	bool armv8 = false;

	// check the args
	if ((NULL == hook) || (NULL == qseecom_scm_call) || (NULL == func)) {
		printk("Error: Bad args\n");
		return;
	}

	// stop if already hooked
	if (hook->hooked) {
		printk("Error: Already hooked\n");
		return;
	}

	// clear the hook information
	memset(hook, 0, sizeof(dz_hook_stub_t));

	// call the architecture check to check its call path is what we expect
    armv8 = dz_is_scm_armv8(dz_mine_is_scm_armv8(qseecom_scm_call));
    if (!armv8) {
        printk("Not Implemented: Hooking non-armv8\n");
        return;
    }

	// mine for all the locations we need
    bl_ins_is_scm_armv8 = aarch64le_mine_bl(qseecom_scm_call, 0x70, 0);
    if (!bl_ins_is_scm_armv8) {
        printk("Error: failed to find is_scm_armv8() bl instruction\n");
        return;
    }
    qseecom_scm_call2 = aarch64le_mine_bl_addr(qseecom_scm_call, 0x70, 1);
    if (!qseecom_scm_call2) {
        printk("Error: failed to find qseecom_scm_call2()\n");
        return;
    }
    bl_ins_qseecom_scm_call2 = aarch64le_mine_bl(qseecom_scm_call, 0x70, 1);
    if (!bl_ins_qseecom_scm_call2) {
        printk("Error: failed to find qseecom_scm_call2() bl instruction\n");
        return;
    }
    ret_ins_qseecom_scm_call2 = aarch64le_mine_ret(qseecom_scm_call, 0x70, 0);
    if (!ret_ins_qseecom_scm_call2) {
        printk("Error: failed to find qseecom_scm_call2() ret instruction\n");
        return;
    }
    bl_ins_scm_call = aarch64le_mine_bl(qseecom_scm_call, 0x70, 2);
    if (!bl_ins_scm_call) {
        printk("Error: failed to find scm_call() bl instruction\n");
        return;
    }
    ret_ins_scm_call = aarch64le_mine_ret(qseecom_scm_call, 0x70, 1);
    if (!ret_ins_scm_call) {
        printk("Error: failed to find scm_call() ret instruction\n");
        return;
    }

	// check that the function we want to match is what we expect it to be
    if (dz_get_qsee_version(qseecom_scm_call) != dz_get_qsee_version2(qseecom_scm_call2)) {
        printk("Error: doesn't look like qseecom_scm_call2() is the right function\n");
        return;
    }

	// place the stub in the unused branch
	// check the free space by measuring the space between the two returns
    hook->stub_addr = (void*)(((unsigned char*)ret_ins_qseecom_scm_call2) + 4);
    free_space = aarch64le_addr_diff(hook->stub_addr, ((unsigned char*)ret_ins_scm_call) + 4);
    if (sizeof(hook->stub) < 24) {
        printk("Error: not enough space for the literal (absolute call address)\n");
        return;
    }

	// we have the hook point and space for the code stub - ready to hook
    printk("Ready to hook\n");

	// the jump into our stub replaces the call to qseecom_scm_call2()
    hook->jmp_addr = bl_ins_qseecom_scm_call2;
    if (!aarch64le_b_ins(hook->jmp, hook->jmp_addr, hook->stub_addr)) {
        printk("Error: failed to generate a jmp instruction\n");
        return;
    }

	// the stub will return to the next instruction after the jump
	// aarch64 has 4 byte instructions :)
    return_addr = (void*)(((unsigned char*)(hook->jmp_addr)) + sizeof(hook->jmp));

	// backup the existing opcodes
	memcpy(hook->jmp_orig, hook->jmp_addr, sizeof(hook->jmp_orig));
	memcpy(hook->stub_orig, hook->stub_addr, sizeof(hook->stub_orig));

	// build a stub
	// register x9 is not used in this code block
    if(!aarch64le_abs_call_stub_x9(hook->stub, sizeof(hook->stub), hook->stub_addr, dz_qseecom_scm_call2_to_call1, return_addr)) {
        printk("Error: failed to generate a call stub\n");
        return;
    }

	// store (globally) the qseecom_scm_call2()
	// need to make actual SCM calls
	dz_qseecom_scm_call2_fn = qseecom_scm_call2;

	// store the detour function and context
	dz_qseecom_scm_call_hook_fn = func;
	dz_qseecom_scm_call_hook_ctx = ctx;

	// debug
    printk("function=%p\n", qseecom_scm_call);
    printk("jmp=%p\n", hook->jmp_addr);
	hexdumpk(hook->jmp_orig, sizeof(hook->jmp_orig));
    hexdumpk(hook->jmp, sizeof(hook->jmp));
    printk("stub=%p\n", hook->stub_addr);
	hexdumpk(hook->stub_orig, sizeof(hook->stub_orig));
    hexdumpk(hook->stub, sizeof(hook->stub));

	// place hook
	hook->hooked = 1;
	if (kw_write(hook->stub_addr, hook->stub, sizeof(hook->stub))) {
		printk("failed to write stub\n");
		return;
	}
	if (kw_write(hook->jmp_addr, hook->jmp, sizeof(hook->jmp))) {
		printk("failed to write jmp\n");
		return;
	}

	// done
	printk("Hooked!!!\n");
}

void dz_unhook_qseecom_scm_call(dz_hook_stub_t *hook) {
	if (NULL == hook) {
		printk("Error: Bad arg");
		return;
	}

	// remove hook
	if (hook->hooked) {
		(void) kw_write(hook->jmp_addr, hook->jmp_orig, sizeof(hook->jmp_orig));
		(void) kw_write(hook->stub_addr, hook->stub_orig, sizeof(hook->stub_orig));
	}

	// remove these (they should no-longer be used)
	dz_qseecom_scm_call2_fn = NULL;
	dz_qseecom_scm_call_hook_fn = NULL;
	dz_qseecom_scm_call_hook_ctx = NULL;

	// reset the structure
	memset(hook, 0, sizeof(dz_hook_stub_t));
}

/*------------------------------------------------------------------*/
// DANGERZONE devices

#define  DZD_DEVICE_NAME "dz"	//  /dev/dz
#define  DZD_CLASS_NAME  "dz"	//  /sys/class/dz

static int dzd_major_number;
static struct class*  dzd_char_class  = NULL;
static struct device* dzd_char_device = NULL;

static void *dzd_qseecom_scm_call = NULL;
static void *dzd_qseecom = NULL;

static int dzd_dev_open(struct inode *, struct file *);
static int dzd_dev_release(struct inode *, struct file *);
static ssize_t dzd_dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dzd_dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations dzd_file_ops =
{
   .open = dzd_dev_open,
   .read = dzd_dev_read,
   .write = dzd_dev_write,
   .release = dzd_dev_release,
};

static int dzd_init(void)
{
	printk("Initializing DZ device\n");

	dzd_qseecom_scm_call = dz_mine_qseecom_scm_call(dz_mine_qseecom_unregister_listener(kallsyms_lookup_name));
	dzd_qseecom = dz_mine_qseecom_control((void*)kallsyms_lookup_name("qseecom_probe"), dz_get_qsee_version(dzd_qseecom_scm_call));
	if (dzd_qseecom)
	{
		printk("DZ device has found qseecom structure\n");
	}
	else
	{
		printk("DZ device failed to find qseecom structure\n");
	}

	// Try to dynamically allocate a major number for the device
	dzd_major_number = register_chrdev(0, DZD_DEVICE_NAME, &dzd_file_ops);
	if (dzd_major_number<0)
	{
		printk("DZ failed to register a major number\n");
		return dzd_major_number;
	}
	printk("DZ registered correctly with major number %d\n", dzd_major_number);

	// Register the device class
	dzd_char_class = class_create(THIS_MODULE, DZD_CLASS_NAME);
	if (IS_ERR(dzd_char_class))
	{
		unregister_chrdev(dzd_major_number, DZD_DEVICE_NAME);
		printk("DZ Failed to register device class\n");
		return PTR_ERR(dzd_char_class);
	}
	printk("DZ device class registered correctly\n");

	// Register the device driver
	dzd_char_device = device_create(dzd_char_class, NULL, MKDEV(dzd_major_number, 0), NULL, DZD_CLASS_NAME);
	if (IS_ERR(dzd_char_device))
	{
		class_destroy(dzd_char_class);
		unregister_chrdev(dzd_major_number, DZD_DEVICE_NAME);
		printk("DZ failed to create the device\n");
		return PTR_ERR(dzd_char_device);
	}
	printk("DZ device class created correctly\n");
	return 0;
}

static void dzd_exit(void)
{
	device_destroy(dzd_char_class, MKDEV(dzd_major_number, 0));
	class_unregister(dzd_char_class);
	class_destroy(dzd_char_class);
	unregister_chrdev(dzd_major_number, DZD_DEVICE_NAME);
	printk("DZ device exit\n");
}

static size_t dzd_dev_app_line_size(struct dz_qseecom_registered_app_list *app)
{
	char buffer[8 + 1 + 8 + 1 + DZ_MAX_APP_NAME_SIZE + 1 + 1];
	snprintf(buffer, sizeof(buffer), "0x%08x 0x%08x %s\n", app->app_id, app->app_arch, app->app_name);
	buffer[sizeof(buffer)-1] = '\0';
	return (size_t)strlen(buffer);
}

static size_t dzd_dev_app_line(char *buffer, size_t buffer_size, struct dz_qseecom_registered_app_list *app)
{
	if (buffer_size == 0) {
		return 0;
	}
	snprintf(buffer, buffer_size, "0x%08x 0x%08x %s\n", app->app_id, app->app_arch, app->app_name);
	buffer[buffer_size-1] = '\0';
	return (size_t)strlen(buffer);
}

struct dzd_dev_app_list {
	size_t pos;
	size_t buffer_size;
	char *buffer;
};

static struct dzd_dev_app_list *dzd_dev_app_list_alloc(void) {
	struct dzd_dev_app_list *app_list = NULL;
	unsigned long flags;
	struct dz_qseecom_registered_app_list *app;
	size_t size = 0;
	size_t pos = 0;
	if (!dzd_qseecom) {
		return NULL;
	}
	spin_lock_irqsave(&(((struct dz_qseecom_control*)dzd_qseecom)->registered_app_list_lock), flags);
	list_for_each_entry(app, &(((struct dz_qseecom_control*)dzd_qseecom)->registered_app_list_head), list) 
	{
		size += dzd_dev_app_line_size(app);
	}
	size += 1;  // size of at least 1, for the nul-termation char
	app_list = (struct dzd_dev_app_list *)kmalloc(sizeof(struct dzd_dev_app_list), GFP_KERNEL);
	if (app_list) {
		app_list->pos = 0;
		app_list->buffer_size = size;
		app_list->buffer = (char*)kmalloc(app_list->buffer_size, GFP_KERNEL);
		if (app_list->buffer)
		{
			list_for_each_entry(app, &(((struct dz_qseecom_control*)dzd_qseecom)->registered_app_list_head), list)
			{
				pos += dzd_dev_app_line(app_list->buffer + pos, app_list->buffer_size - pos, app);
				if (pos > app_list->buffer_size)
				{
					pos = app_list->buffer_size;
				}
			}
			app_list->buffer[app_list->buffer_size-1] = '\0';
			app_list->buffer_size--;  // buffer is nul-terminated, but we don't count the null
		}
		else
		{
			kfree(app_list);
			app_list = NULL;
		}
	}
	spin_unlock_irqrestore(&(((struct dz_qseecom_control*)dzd_qseecom)->registered_app_list_lock), flags);
	return app_list;
}

static void dzd_dev_app_list_free(struct dzd_dev_app_list *app_list)
{
	if (app_list)
	{
		if (app_list->buffer)
		{
			kfree(app_list->buffer);
		}
		kfree(app_list);
	}
}

static int dzd_dev_open(struct inode *inodep, struct file *filep)
{
	struct dzd_dev_app_list *app_list = dzd_dev_app_list_alloc();
	if (app_list)
	{
		filep->private_data = (void*)app_list;
		return 0;
	}
	return -EFAULT;
}

static ssize_t dzd_dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
	struct dzd_dev_app_list *app_list = (struct dzd_dev_app_list *)filep->private_data;
	int error_count;
	size_t remaining;
	if (!app_list)
	{
		return -EFAULT;
	}
	remaining = (app_list->buffer_size - app_list->pos);
	len = len > remaining ? remaining : len;
	error_count = copy_to_user(buffer, app_list->buffer + app_list->pos, len);
	app_list->pos += len;
	if (error_count==0)
	{
		return (ssize_t)len;
	}
	else
	{
		return -EFAULT;
	}
}

static ssize_t dzd_dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
	ssize_t ret = -EFAULT;
	char req_buf[512];
	size_t req_size;
	char *action;
	char *param1;
	char *param2;
	char *param3;
	char *end;
	char *path;
	u32 app_id;
	char app_name[DZ_MAX_APP_NAME_SIZE] = {0};

	// get the user request string
	req_size = len < sizeof(req_buf) ? len : sizeof(req_buf);
	if (0 == req_size) {
		return (ssize_t)len;
	}
	memset(req_buf, 0, sizeof(req_buf));
	ret = copy_from_user(req_buf, buffer, req_size);
	req_buf[req_size - 1] = '\0';
	if (ret) {
		return (ssize_t)-EFAULT;
	}

	// convert to an action
	action = req_buf;
	param1 = strstr(action, ",");
	if (param1) {
		*param1 = '\0';
		param1++;
		param2 = strstr(param1, ",");
		if (param2) {
			*param2 = '\0';
			param2++;
			param3 = strstr(param2, ",");
			if (param3) {
				*param3 = '\0';
				param3++;
				end = strstr(param3, ",");
				if (end) {
					*end = '\0';
				}
			}
		}
	}
	if (0 == memcmp(action, "unload", sizeof("unload")-1)) {
		app_id = dz_app_id(dzd_qseecom, param1);
		if (0 == app_id) {
			app_id = atoi_u32_best_guess(param1);
			if (0 == app_id) {
				return (ssize_t)-EFAULT;
			}
		}
		app_name[0] = '\0';
		dz_app_name(dzd_qseecom, app_id, app_name);
		printk("DANGERZONE unloading 0x%08x [%s]\n", app_id, app_name);
		if(dz_unload_app(dzd_qseecom, dzd_qseecom_scm_call, app_id)) {
			printk("DANGERZONE failed to unload 0x%08x [%s]\n", app_id, app_name);
			return (ssize_t)-EFAULT;
		} else {
			printk("DANGERZONE unloaded 0x%08x [%s]\n", app_id, app_name);
		}
	}
	if (0 == memcmp(action, "load", sizeof("load")-1)) {
		if (NULL == param1) {
			return (ssize_t)-EFAULT;
		}
		strlcpy(app_name, param1, DZ_MAX_APP_NAME_SIZE);
		app_name[DZ_MAX_APP_NAME_SIZE-1] = '\0';
		if (NULL == param2) {
			return (ssize_t)-EFAULT;
		}
		path = param2;
		printk("DANGERZONE loading %s %s\n", app_name, path);
		if(dz_load_app(dzd_qseecom, dzd_qseecom_scm_call, app_name, path)) {
			printk("DANGERZONE failed to load %s\n", app_name);
			return (ssize_t)-EFAULT;
		} else {
			printk("DANGERZONE loaded %s\n", app_name);
		}
	}
	return (ssize_t)len;
}

static int dzd_dev_release(struct inode *inodep, struct file *filep){
	struct dzd_dev_app_list *app_list = (struct dzd_dev_app_list *)filep->private_data;
	if (app_list)
	{
		dzd_dev_app_list_free(app_list);
		filep->private_data = NULL;
	}
	return 0;
}

/*------------------------------------------------------------------*/
// MAIN
static void *qseecom_ptr = NULL;

static dz_hook_stub_t dz_hook = { .hooked = 0 };

static int dz_qseecom_scm_call_hook(void *ctx, DzQseecomScmCallFn orig, u32 svc_id, u32 tz_cmd_id, const void *cmd_buf, size_t cmd_len, void *resp_buf, size_t resp_len) {
	struct dz_qseecom_command_scm_resp *scm_resp = (struct dz_qseecom_command_scm_resp *)resp_buf;
	u32 qseos_cmd_id = dz_qseos_cmd_id(svc_id, tz_cmd_id, cmd_buf);
	int res = orig(svc_id, tz_cmd_id, cmd_buf, cmd_len, resp_buf, resp_len);
	char app_name[DZ_MAX_APP_NAME_SIZE] = {0};
	printk("--------------------------------------------------------\n");
	printk("PID=%d\n", current->pid);
	printk("PROCESS=%s\n", current->comm);
	printk("SVC_ID=0x%02x\n", svc_id);
	printk("SVC_NAME=%s\n", dz_svc_id_name(svc_id));
	printk("SCM_CMD_ID=0x%02x\n", tz_cmd_id);
	printk("SCM_CMD_NAME=%s\n", dz_scm_cmd_name(svc_id, tz_cmd_id));
	if (qseos_cmd_id < DZ_QSEOS_CMD_MAX) {
		printk("QSEOS_CMD_ID=0x%02x\n", qseos_cmd_id);
		printk("QSEOS_CMD_NAME=%s\n", dz_qseos_cmd_name(qseos_cmd_id));
		if (DZ_QSEOS_APP_SHUTDOWN_COMMAND == qseos_cmd_id) {
			u32 app_id = ((struct dz_qseecom_unload_app_ireq *)cmd_buf)->app_id;
			dz_app_name(qseecom_ptr, app_id, app_name);
			printk("QSEOS_CMD_UNLOAD_APP_ID=0x%08x\n", app_id);
			printk("QSEOS_CMD_UNLOAD_APP_NAME=%s\n", app_name);
		}
	}
	printk("RETCODE=%d\n", res);
	printk("RESPONSE_RESULT=0x%08x\n", scm_resp->result);
	printk("RESPONSE_TYPE=0x%08x\n", scm_resp->resp_type);
	printk("RESPONSE_NAME=%s\n", dz_svm_resp_name(scm_resp->resp_type));
	printk("RESPONSE_DATA=0x%08x\n", scm_resp->data);
	printk("REQUEST:\n");
	hexdumpk(cmd_buf, cmd_len);
	printk("RESPONSE:\n");
	hexdumpk(resp_buf, resp_len);
	printk("APPS:\n");
	dz_dump_apps(qseecom_ptr);
	printk("--------------------------------------------------------\n");
	return res;
}

static int dz_init(void)
{
	int result = 0;
    DzQseecomScmCallFn qseecom_scm_call = NULL;
	void *qseecom = NULL;

	printk("--------------------------------------------------------\n");
    printk("DANGERZONE Init (5)\n");
	result =dzd_init();
	if (result) {
		return result;
	}
    qseecom_scm_call = (DzQseecomScmCallFn)dz_mine_qseecom_scm_call(dz_mine_qseecom_unregister_listener(kallsyms_lookup_name));
	qseecom = dz_mine_qseecom_control((void*)kallsyms_lookup_name("qseecom_probe"), dz_get_qsee_version(qseecom_scm_call));
	qseecom_ptr = qseecom;
    printk("qsee_version=0x%x\n", dz_get_qsee_version(qseecom_scm_call));
    printk("armv8=%s\n", dz_is_scm_armv8(dz_mine_is_scm_armv8(qseecom_scm_call)) ? "true" : "false");
    dz_hook_qseecom_scm_call(&dz_hook, qseecom_scm_call, dz_qseecom_scm_call_hook, NULL);
    printk("--------------------------------------------------------\n");
	printk("qsee_version=0x%x\n", dz_get_qsee_version(qseecom_scm_call));
	dz_dump_apps(qseecom);
    return result;
}

static void dz_exit(void)
{
	printk("--------------------------------------------------------\n");
    printk("DANGERZONE Exit\n");
	dzd_exit();
	dz_unhook_qseecom_scm_call(&dz_hook);
	printk("--------------------------------------------------------\n");
}

module_init(dz_init);
module_exit(dz_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Peter Rankin, 2018");
MODULE_DESCRIPTION("DANGERZONE");
